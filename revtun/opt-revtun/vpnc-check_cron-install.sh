#!/bin/bash

CRON_15MIN_DIR="/etc/periodic/15min"
VPNC_CHECK_SH="/opt/revtun/vpnc-check.sh"
VPNC_CHECK_CRONJOB="$CRON_15MIN_DIR/vpnc-check"

CROND_LOG_LEVEL=${CROND_LOG_LEVEL:-8}

checkCronRun() {
	if [ -z $(pgrep crond) ]; then # crond is not running, let's start it
		crond -L /var/log/crond -l $CROND_LOG_LEVEL
	else # crond already running
		echo; echo crond already running.
	fi
}

if [ -z $VPNC_PING_CHECK ]; then
	echo; echo "Skipping cronjob install as \$VPNC_PING_CHECK is not set."
else
	if [ -e "$CRON_15MIN_DIR" ]; then
		if [ -e "$VPNC_CHECK_CRONJOB" ]; then
			checkCronRun
		elif [ -f "$VPNC_CHECK_SH" ]; then
			echo; echo "Installing VPNC Check cronjob at \"$VPNC_CHECK_CRONJOB\"..."
			ln -s "$VPNC_CHECK_SH" "$VPNC_CHECK_CRONJOB"
			checkCronRun
		else
			echo; echo "ERROR: File not found: \"$VPNC_CHECK_SH\""
			exit 1
		fi
	else
		echo; echo "ERROR: Directory does not exist: \"$CRON_15MIN_DIR\""
		exit 1
	fi
fi

$VPNC_CHECK_SH --no-log

