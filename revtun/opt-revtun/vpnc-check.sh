#!/bin/bash

VPNC_PING_CHECK_LOG="${VPNC_PING_CHECK_LOG:-/var/log/vpnc-check}"
PING_SECONDS=${PING_SECONDS:-5}
SSH_CHECK_LOG="${SSH_CHECK_LOG:-${VPNC_PING_CHECK_LOG}}"
REVTUN_SH="${REVTUN_SH:-/opt/revtun/revtun.sh}"

LOG=true
SSH=true

for param in "$@"; do
	case "$param" in
		"--stdout"|"--no-log")
			LOG=false
			;;
		"--no-ssh")
			SSH=false
			;;
		"--help")
			echo "Options: --stdout or --no-log (use stdout instead of log file); --no-ssh (skip ssh)"
			exit
			;;
		*)
			;;
	esac
done

timestampEcho() {
	echo -n "$(date -I'seconds') " ; echo "$@"
}

checkVpnc() {
	echo

	if [ -z "$VPNC_PING_CHECK" ]; then
		timestampEcho "Skipping VPNC check as \$VPNC_PING_CHECK is not set."
	elif [ -s "$VPNC_CONF" ]; then
		timestampEcho "VPNC check: ping $VPNC_PING_CHECK for $PING_SECONDS seconds..."
		
		ping -q -W 3 -w $PING_SECONDS $VPNC_PING_CHECK > /dev/null
		
		if [ $? == 0 ]; then
			#PING_OK=true
			timestampEcho "OK"
		else
			PING_FAIL=true
			timestampEcho "Ping ERROR! Disconnecting VPNC if still running..."
			vpnc-disconnect
			timestampEcho "Running VPNC with config file \"$VPNC_CONF\" ..."
			vpnc $VPNC_CONF || exit 999
		fi
	else
		timestampEcho "ERROR: VPNC config file \"$VPNC_CONF\" not found or empty."
		exit 99
	fi
}

checkSsh() {
	if [ "$PING_FAIL" == true ]; then
		timestampEcho "Killing all \"ssh -R\" ..."
		pkill -f "ssh -R"
		RUN_SSH=true
	else
		if [ -z "$(pgrep -f 'ssh -R')" ]; then
			timestampEcho "SSH with reverse tunnels is not running."
			RUN_SSH=true
		else
			timestampEcho "SSH with reverse tunnels already running."
		fi
	fi
	
	if [ "$RUN_SSH" == true ]; then
		timestampEcho "Running $REVTUN_SH ..."
		$REVTUN_SH
	fi
}

if [ "$LOG" == true ]; then
	checkVpnc >> $VPNC_PING_CHECK_LOG 2>&1
else
	checkVpnc
fi

if [ "$SSH" == true ]; then
	if [ "$LOG" == true ]; then
		checkSsh >> $SSH_CHECK_LOG 2>&1
	else
		checkSsh
	fi
fi

#{
#} >> $VPNC_PING_CHECK_LOG 2>&1

