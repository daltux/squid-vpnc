#!/bin/bash

SSH_KEY=/opt/revtun/ssh_key/id_rsa
REVTUN_CONF=/opt/revtun/revtun.conf

readConf() {
	sed -e 's/[[:space:]]*#.*// ; /^[[:space:]]*$/d' $REVTUN_CONF | {
		while read lin ; do
			[[ -n "$result" ]] && result+=' '
			result+="-R $lin"
		done
		echo $result
	}
}

if [ -n "$(sysctl net.ipv4.tcp_mtu_probing | grep 0)" ]; then
	echo; echo "WARNING: Kernel parameter net.ipv4.tcp_mtu_probing is currently 0 (zero), which means TCP Packetization-Layer Path MTU Discovery is disabled. Please set it to 1 (disabled by default but enabled when ICMP blackhole is detected) or 2 (always enabled). Trying to set it to 1 ..."
	sysctl -w net.ipv4.tcp_mtu_probing=1 && echo OK || echo "If you have trouble with some connections, maybe the container will have to be run with --privileged mode."
fi

if [ -z "$SSH_HOST_USER" ]; then
	echo; echo "SSH user is unknown. Please set environment variable SSH_HOST_USER"
	exit 10
fi

if [ -z "$SSH_HOST_ADDR" ]; then
	echo; echo "Trying to get default gateway address and supposing it is the host address."
	SSH_HOST_ADDR=$(/sbin/ip route|awk '/default/ { print $3 }')
	if [ -z "$SSH_HOST_ADDR" ]; then
		echo "SSH host address is still unknown. Please set environment variable SSH_HOST_ADDR"
		exit 20
	else
		echo SSH host address: $SSH_HOST_ADDR
	fi
fi

[ -z "$SSH_HOST_PORT" ] && SSH_HOST_PORT=22

/opt/revtun/vpnc-check.sh --stdout --no-ssh
#if [ -z "$VPNC_CONF" ]; then
#	echo; echo "WARNING: Skipping VPNC as \$VPNC_CONF (VPNC config file) is not set."
#elif [ -s "$VPNC_CONF" ]; then
#	echo; echo "Running VPNC with config file \"$VPNC_CONF\" ..."
#	vpnc $VPNC_CONF || exit 999
#else
#	echo; echo ERROR: VPNC config file \"$VPNC_CONF\" not found or empty.
#	exit 99
#fi

if [ -f "$REVTUN_CONF" ]; then
	echo; echo "Reading file \"$REVTUN_CONF\" with reverse tunnels settings..."
	REVTUNNELS=$(readConf)
	
	if [ -z "$REVTUNNELS" ]; then
		echo; echo "WARNING: Skipping ssh as no reverse tunnel has been set."
	else
		export SSH_CMD="ssh $REVTUNNELS -o StrictHostKeyChecking=no -o ExitOnForwardFailure=yes -i $SSH_KEY -p $SSH_HOST_PORT -fgNTv $SSH_HOST_USER@$SSH_HOST_ADDR"
		
		if [ -n "$REVTUN_USER" ] && [ "$REVTUN_USER" != "$USER" ]; then
			echo; echo "Running SSH command with user \"$REVTUN_USER\": \"$SSH_CMD\" ... "
			su $REVTUN_USER -c '$SSH_CMD' && echo OK || ( ERROR_CODE=$? && echo "ERROR CODE $ERROR_CODE" && exit $ERROR_CODE )
		else
			echo; echo "Running SSH command: \"$SSH_CMD\" ... "
			$SSH_CMD && echo OK || ( ERROR_CODE=$? && echo "ERROR CODE $ERROR_CODE" && exit $ERROR_CODE )
		fi
	fi
else
	echo; echo "WARNING: Reverse tunnels settings file \"$REVTUN_CONF\" not found."
fi

