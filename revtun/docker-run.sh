#!/bin/bash

# Set which port is going to be opened on host for container sshd listen
CONTAINER_SSHD_PORT=2200

# Set which user and address the container is going to use to connect to the host
SSH_HOST_USER=$USER
SSH_HOST_ADDR=172.17.0.1

AUTHORIZED_KEYS_FILE=/home/my_user/revtun/authorized_keys

# Directory to be used by SSHD configuration
ETC_SSH_DIR=/home/my_user/revtun/etc-ssh/

REVTUN_CONF=/home/my_user/revtun/revtun.conf

SSH_KEY_DIR=/home/my_user/revtun/ssh_key/

# Optional VPNC configuration file if you want it to open a VPNC connection in the container:
HOST_VPNC_CONF=/home/my_user/vpnc.conf

# Some host in the VPN for checking by ping if VPNC is working.
VPNC_PING_CHECK=""

HOST_VPNC_CHECK_LOG=/tmp/vpnc-check.log
CONTAINER_VPNC_CHECK_LOG=/var/log/vpnc-check

CONTAINER_VPNC_CONF=/etc/vpnc/vpnc.conf

if [ -n "$(sysctl net.ipv4.tcp_mtu_probing | grep 0)" ]; then
        echo; echo "WARNING: Kernel parameter net.ipv4.tcp_mtu_probing is currently 0 (zero), which means TCP Packetization-Layer Path MTU Discovery is disabled. Please set it to 1 (disabled by default but enabled when ICMP blackhole is detected) or 2 (always enabled)."
	read -n 1 -t 10
fi

# --cap-add net_admin is needed for vpnc
# --privileged mode is needed if we must set kernel parameter net.ipv4.tcp_mtu_probing

docker run --name revtun \
	-p $HOST_SQUID_PORT:3128 \
	-p $CONTAINER_SSHD_PORT:22 \
	--env SSH_HOST_USER=$SSH_HOST_USER \
	--env SSH_HOST_ADDR=$SSH_HOST_ADDR \
	--env VPNC_CONF=$CONTAINER_VPNC_CONF \
	--env VPNC_PING_CHECK=$VPNC_PING_CHECK \
	--env VPNC_PING_CHECK_LOG=$CONTAINER_VPNC_CHECK_LOG \
	--volume $HOST_VPNC_CONF:$CONTAINER_VPNC_CONF:ro \
	--volume $HOST_VPNC_CHECK_LOG:$CONTAINER_VPNC_CHECK_LOG \
	--volume $AUTHORIZED_KEYS_FILE:/etc/authorized_keys/revtun \
	--volume $ETC_SSH_DIR:/etc/ssh/ \
	--volume $REVTUN_CONF:/opt/revtun/revtun.conf \
	--volume $SSH_KEY_DIR:/opt/revtun/ssh_key/:ro \
	--cap-add net_admin \
	--privileged \
	daltux/revtun $@

