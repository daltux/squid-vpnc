#!/bin/sh

CONF_NAME="squid.conf"
DEFAULT_CONF_DIR="/etc/squid"
DEFAULT_CONF="$DEFAULT_CONF_DIR/$CONF_NAME"

TMP_CONF_DIR="/tmp/my-etc-squid"
TMP_CONF="$TMP_CONF_DIR/$CONF_NAME"

if [ -s "$TMP_CONF" ]; then
	if [ -e $DEFAULT_CONF_DIR ]; then
		echo; echo "Backing up existing \"$DEFAULT_CONF_DIR\"..."
		mv -fv "$DEFAULT_CONF_DIR" "${DEFAULT_CONF_DIR}_$(date -Iseconds).bak"
	fi

	echo; echo "Copying \"$TMP_CONF_DIR\" to \"$DEFAULT_CONF_DIR\"..."
	cp -RLfv "$TMP_CONF_DIR" "$DEFAULT_CONF_DIR"
elif [ -s "$DEFAULT_CONF" ]; then
	echo; echo "Copying \"$DEFAULT_CONF_DIR\" to \"$TMP_CONF_DIR\"..."
	cp -RLv "$DEFAULT_CONF_DIR/*" "$TMP_CONF_DIR"
fi

echo; echo "Running squid for creating cache directories..."
squid --foreground -z

if [ $? -eq 0 ]; then
	echo; echo "Running squid..."
	exec /usr/sbin/squid -YCd 1
fi

