**Source code repository of [my custom Docker images](https://hub.docker.com/u/daltux)
based on [Alpine Linux](https://hub.docker.com/_/alpine)**

Each directory has a `docker-run.sh` file which as example to run the image.

---

## mysquid

Squid web proxy server with optional VPNC (Cisco VPN) connection and reverse ssh tunnels.

## pyserv

A simplistic HTTP server based on [Python 2 `SimpleHTTPServer`](https://docs.python.org/2/library/simplehttpserver.html), for serving a directory with static files.
Its goal is serving a [proxy auto-config file](https://en.wikipedia.org/wiki/Proxy_auto-config) using as little resources as possible.

## alpine-brt
[Alpine](https://hub.docker.com/_/alpine) configured to use `America/Sao_Paulo` (UTC-3) timezone.

