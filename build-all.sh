#!/bin/bash

docker build --tag daltux/alpine-brt  alpine-brt/
docker build --tag daltux/pyserv      pyserv/
docker build --tag daltux/vpnc        vpnc/
docker build --tag daltux/revtun      revtun/
docker build --tag daltux/mysquid     mysquid/
docker build --tag daltux/mysquid-brt mysquid-brt/

