#!/bin/bash

# File with VPNC configuration.
# You can use Network Manager to export a pcf file and convert it with pcf2vpnc
# Thanks to Josh Berkus for the tip:
# http://www.databasesoup.com/2015/07/using-docker-to-isolate-vpn-clients.html
VPNC_CONF=/home/my_user/my_vpnc.conf

docker run -it --name vpnc \
	--cap-add net_admin \
	--volume $VPNC_CONF:/etc/vpnc/vpnc.conf:ro \
	daltux/vpnc

