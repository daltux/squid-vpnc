#!/bin/bash

HOST_PORT=8088
HTDOCS_DIR=/path_to_the_directory_to_be_served/

docker run --name pyserv \
	-p $HOST_PORT:80 \
	--volume $HTDOCS_DIR:/opt/htdocs:ro \
	daltux/pyserv

