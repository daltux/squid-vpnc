import SimpleHTTPServer
import SocketServer

PORT = 80

class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    pass

Handler.extensions_map['.pac'] = 'application/x-ns-proxy-autoconfig'

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "serving at port", PORT
httpd.serve_forever()

